# Author: john.c.dryden@gmail.com
# 

from __future__ import print_function
from IPython.display import display, Image
# Load up matplotlib and pyplot for plotting things.
%matplotlib inline
import matplotlib.pyplot as plt
import json
import gdal
import math
import pandas as pd
import numpy as np


import descarteslabs as dl

# define shape, collect metadata - one object

mycoord = [34.024029,-5.1418167]

dlkey_small = dl.raster.dlkey_from_latlon(mycoord[0],mycoord[1], 10.0, 128, 16)

sources = dl.metadata.search(const_id=["S2A"], #NOTE: RE has NDVI, not NDWI
                                start_time='2016-01-01',
                                end_time='2016-02-01',
                                geom=json.dumps(dlkey_small['geometry']),
)

# calculate avg and stdev NDVI for the whole parcel, for each scene
# for source in sources:
#     this_scene = source['id']
stack=None    
for i, source in enumerate(sources['features']):
    this_scene = source['id']
    arr, meta = dl.raster.ndarray(
        this_scene,
        resolution=dlkey_small['properties']['resolution'],
        bounds=dlkey_small['properties']['outputBounds'],
        srs=dlkey_small['properties']['cs_code'], 
        bands=['ndvi','alpha'], # try swapping 'red' with 'ndvi'
        scales=[None, [0,1]], # ndvi scale should be [32768,65535]
        data_type='UInt16',
    )
    if stack is None:
        stack = np.zeros((arr.shape[0], arr.shape[1], len(sources['features'])))
    stack[:,:,i] = arr[:,:,0]
   # plt.imshow(arr[:,:,0])
    print(arr.shape, np.min(arr[:,:,0]))
    
#sources['features']

# read in geojson

with open('FINAL SHAPEFILES.geojson') as f:
    data = json.load(f)

ndvis = []
# iterate over each feature
for feature in data['features'][1:]:
    
    sources = dl.metadata.search(const_id=["S2A"], #NOTE: RE has NDVI, not NDWI
                                start_time='2016-01-01',
                                end_time='2016-02-01',
                                geom=json.dumps(feature['geometry'])
                                #geom=json.dumps(dlkey['geometry']),
                                )
    stack=None    
    print(feature['geometry'])
    for i, source in enumerate(sources['features'][:1]):
        this_scene = source['id']
        try:
            arr, meta = dl.raster.ndarray(
                this_scene,
                resolution= 30.0,
                bands=['ndvi','alpha'], # try swapping 'red' with 'ndvi',
                shape=feature['geometry'],
                scales=[None, [0,1]], # ndvi scale should be [32768,65535],
                data_type='UInt16',
            )
        except:
            continue
        #plt.figure()
        #arr[:,:,0][arr[:,:,1] == 0] = 0
        #plt.imshow(arr[:,:,0])
        #if stack is None:
        #    stack = np.zeros((arr.shape[0], arr.shape[1], len(sources['features'])))
        #stack[:,:,i] = arr[:,:,0]
        marr = np.ma.masked_array(arr[:,:,0], arr[:,:,1]==0)
        #plt.imshow(marr)
        mean_ndvi = np.mean(marr)
        #print(mean_ndvi)

    feature['properties']['ndvi'] = mean_ndvi
    #parcel = np.mean(mean_ndvi)
    #ndvis.append(parcel)

print(ndvis)

from copy import deepcopy
orig_data = deepcopy(data)

#normalize 
final_data = deepcopy(orig_data)
for feature in final_data['features']:
    if 'ndvi' not in feature['properties']:
        feature['properties']['ndvi'] = 0.
    
    feature['properties']['ndvi'] = (feature['properties']['ndvi'] /32768.0) -1
    feature['properties']['ndvi'] = float(feature['properties']['ndvi'])
with open('newfile.geojson','w') as f:
    json.dump(data,f,indent=2)